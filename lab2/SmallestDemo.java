public class SmallestDemo {

    public static void main(String[] args){

    	int value1 =  Integer.parseInt(args[0]);
	int value2 =  Integer.parseInt(args[1]);
        int value3 =  Integer.parseInt(args[2]);
        int result;

        boolean small = value1 < value2;

        result = small ? value1 : value2;

	boolean smallest = result < value3;

	result = smallest ? result : value3;

        System.out.println(result);	
    }
}
